# My Booking Service

## 1 Build

```bash
docker-compose build
```

## 2 Deployment

```bash
docker-compose up -d
```

Now, api is up and can be accessed at [http://localhost:11111](http://localhost:11111)

You can access swagger api for api documention at [http://localhost:11111/docs](http://localhost:11111/docs)

from tests_utils.client import client


def get_token() -> str:
    response = client.post(
        "/auth/login",
        data={"username": "admin@booking.com",
              "password": "test"}
    )
    data = response.json()
    return data["token"]

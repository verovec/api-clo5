from typing import Optional
from fastapi import APIRouter, Depends, HTTPException, Header
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from database.dependencies import get_db, get_user, need_authentication
from database.models.Token import Token
from database.models.User import User
from database.schemas import User as UserSchemas
from database.schemas import Token as TokenSchemas
from services.token_service import TokenService

router: APIRouter = APIRouter()


@router.post(path="/login", response_model=TokenSchemas.Token)
def create_login(user_infos: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)) -> Token:
    user: Optional[User] = db.query(User).filter_by(
        email=user_infos.username).first()
    if user is None or user.check_password(password=user_infos.password) is False:
        raise HTTPException(
            status_code=404, detail="Email ou mot de passe incorrect")
    try:
        return TokenService.generateUserToken(user_id=user.id, db=db)
    except HTTPException as exception:
        raise exception


@router.get(path="/check", response_model=UserSchemas.User, dependencies=[Depends(need_authentication)])
def read_check(user: User = Depends(get_user)):
    return user


@router.get(path="/logout", response_model=bool, dependencies=[Depends(need_authentication)])
def read_logout(x_api_token: str = Header(...), db: Session = Depends(get_db)) -> bool:
    token_db: Optional[Token] = db.query(Token).filter_by(token=x_api_token).order_by(
        Token.ut_created_at.desc()).first()
    if token_db is None:
        raise HTTPException(
            status_code=401, detail="Token invalide")
    return TokenService.removeToken(token_id=token_db.id, db=db)

from collections import Counter
from typing import Optional
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from database.dependencies import get_db, only_manager
from database.models.Hotel import Hotel, HotelHasExtra, HotelHasRoom
from database.models.Room import Room
from database.models.Extra import Extra
from database.schemas import Hotel as HotelSchemas

router: APIRouter = APIRouter()


@router.get(path="", response_model=list[HotelSchemas.Hotel])
def read_hotels(db: Session = Depends(get_db)) -> list[Hotel]:
    return db.query(Hotel).all()


@router.get(path="/{hotel_id}", response_model=HotelSchemas.Hotel)
def read_hotel(hotel_id: int, db: Session = Depends(get_db)) -> Hotel:
    hotel = db.query(Hotel).get(ident=hotel_id)

    if hotel is None:
        raise HTTPException(
            status_code=404, detail="L'hôtel n'existe pas")

    return hotel


@router.post(path="", response_model=HotelSchemas.Hotel, dependencies=[Depends(only_manager)])
def create_hotel(hotel: HotelSchemas.HotelCreate, db: Session = Depends(get_db)) -> Hotel:
    db_hotel: Hotel = Hotel(address=hotel.address, phone=hotel.phone)

    rooms_ids: Counter = Counter(hotel.rooms_ids)
    for room_id in rooms_ids.most_common():
        room: Optional[Room] = db.query(Room).get(ident=room_id[0])
        if room is None:
            raise HTTPException(
                status_code=404, detail=f"La chambre {room_id[0]} n'existe pas")
        db_hotel.rooms.append(HotelHasRoom(
            room_id=room_id[0], number=room_id[1]))

    extras_ids: Counter = Counter(hotel.extras_ids)
    for extra_id in extras_ids.most_common():
        extra: Optional[Extra] = db.query(Extra).get(ident=extra_id[0])
        if extra is None:
            raise HTTPException(
                status_code=404, detail=f"L'extra {extra_id[0]} n'existe pas")
        db_hotel.extras.append(HotelHasExtra(
            extra_id=extra_id[0], number=extra_id[1]))

    db.add(instance=db_hotel)
    db.commit()
    db.refresh(instance=db_hotel)

    return db_hotel

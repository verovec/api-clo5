from collections import Counter
from typing import Optional
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from database.dependencies import get_db, get_user
from database.models.User import User
from database.models.Booking import Booking, BookingHasRoom, BookingHasExtra
from database.models.Room import Room
from database.models.Extra import Extra
from database.models.Hotel import Hotel
from database.schemas import Booking as BookingSchemas
from services.booking_service import BookingService

router: APIRouter = APIRouter()


@router.get(path="", response_model=list[BookingSchemas.Booking])
def read_bookings(db: Session = Depends(get_db), user: User = Depends(get_user)) -> list[Booking]:
    bookings = db.query(Booking)

    if user.role.name != "manager" and user.role.name != "admin":
        bookings = bookings.filter_by(user_id=user.id)

    return bookings.all()


@router.get(path="/{booking_id}", response_model=BookingSchemas.Booking)
def read_booking(booking_id: int, db: Session = Depends(get_db)) -> Booking:
    booking = db.query(Booking).get(ident=booking_id)

    if booking is None:
        raise HTTPException(
            status_code=404, detail="La réservation n'existe pas")

    return booking


@router.post(path="", response_model=BookingSchemas.Booking)
def create_booking(booking: BookingSchemas.BookingCreate, db: Session = Depends(get_db), user: User = Depends(get_user)) -> Booking:
    hotel: Optional[Hotel] = db.query(Hotel).get(ident=booking.hotel_id)
    if hotel is None:
        raise HTTPException(
            status_code=404, detail=f"L'hôtel {booking.hotel_id} n'existe pas")

    db_booking: Booking = Booking(
        user_id=user.id, hotel_id=booking.hotel_id, begin_date=booking.begin_date, end_date=booking.end_date)

    for room_id, number in Counter(booking.rooms_ids).items():
        room: Optional[Room] = db.query(Room).get(ident=room_id)
        if room is None:
            raise HTTPException(
                status_code=404, detail=f"La chambre {room_id} n'existe pas")
        db_booking.rooms.append(BookingHasRoom(
            booking=db_booking, room=room, number=number))

    for extra_id, number in Counter(booking.extras_ids).items():
        extra: Optional[Extra] = db.query(Extra).get(ident=extra_id)
        if extra is None:
            raise HTTPException(
                status_code=404, detail=f"La chambre {extra_id} n'existe pas")
        db_booking.extras.append(BookingHasExtra(
            booking=db_booking, extra=extra, number=number))

    intersect, intersect_detail = BookingService.is_booking_inter_date(
        db, booking)
    if intersect:
        raise HTTPException(
            status_code=409, detail=f"Impossible de réserver, {intersect_detail}")

    db.add(instance=db_booking)
    db.commit()
    db.refresh(instance=db_booking)

    return db_booking

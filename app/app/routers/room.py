from typing import Optional
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from database.dependencies import get_db, only_manager
from database.models.Room import Room
from database.schemas import Room as RoomSchemas

router: APIRouter = APIRouter()


@router.get(path="", response_model=list[RoomSchemas.Room])
def read_rooms(db: Session = Depends(get_db)) -> list[Room]:
    return db.query(Room).all()


@router.get(path="/{room_id}", response_model=RoomSchemas.Room)
def read_room(room_id: int, db: Session = Depends(get_db)) -> Room:
    room = db.query(Room).get(ident=room_id)

    if room is None:
        raise HTTPException(
            status_code=404, detail="La chambre n'existe pas")

    return room


@router.post(path="", response_model=RoomSchemas.Room, dependencies=[Depends(only_manager)])
def create_room(room: RoomSchemas.RoomCreate, db: Session = Depends(get_db)) -> Room:
    db_room: Room = Room(**room.dict())

    db.add(instance=db_room)
    db.commit()
    db.refresh(instance=db_room)

    return db_room

from tests_utils.auth import get_token
from tests_utils.client import client


def test_get_wrong_booking():
    token = get_token()

    response = client.get(
        "/booking/999999",
        headers={
            "x-api-token": token
        }
    )

    assert response.status_code == 404
    data = response.json()
    assert "detail" in data


def test_get_bookings_empty():
    token = get_token()

    response = client.get(
        "/booking",
        headers={
            "x-api-token": token
        }
    )

    assert response.status_code == 200
    data = response.json()
    assert len(data) == 0


def test_create_booking():
    token = get_token()

    response = client.post(
        "/booking",
        headers={
            "x-api-token": token
        },
        json={
            "hotel_id": 1,
            "rooms_ids": [5],
            "extras_ids": [],
            "begin_date": "2022-01-01T09:00:00.000Z",
            "end_date": "2022-01-07T09:00:00.000Z"
        }
    )

    print(response.text)

    assert response.status_code == 200
    data = response.json()
    assert "id" in data


def test_get_booking():
    token = get_token()

    response = client.get(
        "/booking/1",
        headers={
            "x-api-token": token
        }
    )

    assert response.status_code == 200
    data = response.json()
    assert "id" in data


def test_get_bookings():
    token = get_token()

    response = client.get(
        "/booking",
        headers={
            "x-api-token": token
        }
    )

    assert response.status_code == 200
    data = response.json()
    assert len(data) > 0

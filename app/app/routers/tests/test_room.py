from tests_utils.auth import get_token
from tests_utils.client import client


def test_get_room():
    token = get_token()

    response = client.get(
        "/room/1",
        headers={
            "x-api-token": token
        }
    )

    assert response.status_code == 200
    data = response.json()
    assert "id" in data


def test_get_wrong_room():
    token = get_token()

    response = client.get(
        "/room/999999",
        headers={
            "x-api-token": token
        }
    )

    assert response.status_code == 404
    data = response.json()
    assert "detail" in data


def test_get_rooms():
    token = get_token()

    response = client.get(
        "/room",
        headers={
            "x-api-token": token
        }
    )

    assert response.status_code == 200
    data = response.json()
    assert len(data) > 0

from tests_utils.client import client


def test_login():
    response = client.post(
        "/auth/login",
        data={"username": "admin@booking.com",
              "password": "test"}
    )

    assert response.status_code == 200
    data = response.json()
    assert "token" in data


def test_bad_login():
    response = client.post(
        "/auth/login",
        data={"username": "bad@booking.com",
              "password": "test"}
    )

    assert response.status_code == 404
    data = response.json()
    assert "detail" in data

import time
from typing import Optional
from uuid import uuid4

from fastapi import HTTPException
from sqlalchemy.orm import Session

from database.models.User import User
from database.models.Token import Token


TOKEN_EXPIRATION_TIME: int = 60 * 60 * 24 * 1000  # 1000 days


class TokenService():
    @staticmethod
    def generateUserToken(user_id: int, db: Session) -> Token:
        """
        Creates a token for a specific user.
        Removes any token previously created for the user.
        """
        user: Optional[User] = db.query(User).get(ident=user_id)
        if user is None:
            raise HTTPException(
                status_code=404, detail="Impossible de trouver l'utilisateur")

        timestamp: float = time.time()
        token_value: str = str(uuid4())
        expires_at: int = int(timestamp + TOKEN_EXPIRATION_TIME)
        token: Token = Token(
            token=token_value,
            user_id=user.id,
            ut_created_at=timestamp,
            ut_expires_at=expires_at
        )
        TokenService.clearUserTokens(user_id=user.id, db=db)
        db.add(instance=token)
        db.commit()

        return token

    @staticmethod
    def renewToken(token_id: int, db: Session) -> Optional[Token]:
        """
        Renews a token for the maximum expiration time.
        """
        token: Optional[Token] = db.query(Token).get(ident=token_id)
        if token is not None:
            timestamp: float = time.time()
            expires_at: int = int(timestamp + TOKEN_EXPIRATION_TIME)
            token.ut_expires_at = expires_at  # type: ignore

            db.commit()

        return token

    @staticmethod
    def removeToken(token_id: int, db: Session) -> bool:
        """
        Renews a token for the maximum expiration time.
        """
        db.query(Token).filter_by(id=token_id).delete()
        db.commit()

        return True

    @staticmethod
    def clearUserTokens(user_id: int, db: Session) -> None:
        db.query(Token).filter(Token.user_id == user_id).delete()
        db.commit()

    @staticmethod
    def getValidToken(token_value: str, db: Session) -> Optional[Token]:
        return db.query(Token).filter(Token.token == token_value).filter(Token.ut_expires_at >= time.time()).first()

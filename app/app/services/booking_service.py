from typing import Optional, Tuple
from sqlalchemy import and_, or_
from sqlalchemy.orm import Session

from database.models.Booking import Booking
from database.models.Booking import Hotel
from database.schemas import Booking as BookingSchemas


class BookingService():
    @staticmethod
    def is_booking_inter_date(db: Session, booking: BookingSchemas.BookingCreate) -> Tuple[bool, Optional[str]]:
        hotel: Hotel = db.query(Hotel).get(booking.hotel_id)
        if hotel is None:
            return True, "L'hôtel n'existe pas"

        bookings = db.query(Booking).filter(and_(
            Booking.hotel_id == booking.hotel_id,
            or_(
                and_(
                    booking.begin_date >= Booking.begin_date,
                    booking.begin_date <= Booking.end_date
                ),
                and_(
                    booking.end_date >= Booking.begin_date,
                    booking.end_date <= Booking.end_date
                ),
                and_(
                    booking.begin_date <= Booking.begin_date,
                    booking.end_date >= Booking.end_date
                )
            )
        ))

        available_extras: dict = {}
        available_rooms: dict = {}

        for hotelHasExtra in hotel.extras:
            available_extras[hotelHasExtra.extra_id] = hotelHasExtra.number
        for hotelHasRoom in hotel.rooms:
            available_rooms[hotelHasRoom.room_id] = hotelHasRoom.number

        for booking_db in bookings:
            for bookingHasExtra in booking_db.extras:
                if bookingHasExtra.extra_id in available_extras:
                    available_extras[bookingHasExtra.extra_id] -= bookingHasExtra.number
            for bookingHasRoom in booking_db.rooms:
                if bookingHasRoom.room_id in available_rooms:
                    available_rooms[bookingHasRoom.room_id] -= bookingHasRoom.number

        for room_id in booking.rooms_ids:
            if room_id not in available_rooms or available_rooms[room_id] <= 0:
                return True, "Une ou plusieurs chambres ne sont pas disponibles"
        for extra_id in booking.extras_ids:
            if extra_id not in available_extras or available_extras[extra_id] <= 0:
                return True, "Un ou plusieurs extras ne sont pas disponibles"

        return False, None

from typing import Generator, Optional
from fastapi import Depends, HTTPException, Header
from sqlalchemy.orm import Session

from services.token_service import TokenService
from database.database import SessionLocal
from database.models.User import User
from database.models.Token import Token


def get_db() -> Generator[Session, None, None]:
    db: Session = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_user(x_api_token: str = Header(...), db: Session = Depends(get_db)) -> User:
    token_db: Optional[Token] = TokenService.getValidToken(
        token_value=x_api_token, db=db)
    if token_db is None:
        raise HTTPException(status_code=401, detail="Connexion requise")
    return token_db.user


async def only_manager(user: User = Depends(get_user)):
    if user.role.name != "manager" and user.role.name != "admin":
        raise HTTPException(status_code=403, detail="Permission refusée")


async def need_authentication(x_api_token: str = Header(...), db: Session = Depends(get_db)) -> None:
    token_db: Optional[Token] = TokenService.getValidToken(
        token_value=x_api_token, db=db)
    if token_db is None:
        raise HTTPException(status_code=401, detail="Connexion requise")

from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Table, func
from sqlalchemy.orm import relationship

from database.database import Base
from database.models.Room import Room
from database.models.Extra import Extra


class HotelHasExtra(Base):
    __tablename__ = "HotelHasExtra"

    id = Column(type_=Integer, primary_key=True, index=True)
    hotel_id = Column(Integer, ForeignKey(column="Hotel.id"))
    extra_id = Column(Integer, ForeignKey(column=Extra.id))
    number = Column(type_=Integer)

    hotel = relationship(argument="Hotel", back_populates="extras")
    extra = relationship(argument=Extra)


class HotelHasRoom(Base):
    __tablename__ = "HotelHasRoom"

    id = Column(type_=Integer, primary_key=True, index=True)
    hotel_id = Column(Integer, ForeignKey(column="Hotel.id"))
    room_id = Column(Integer, ForeignKey(column=Room.id))
    number = Column(type_=Integer)

    hotel = relationship(argument="Hotel", back_populates="rooms")
    room = relationship(argument=Room)


class Hotel(Base):
    __tablename__ = "Hotel"

    id = Column(type_=Integer, primary_key=True, index=True)
    address = Column(type_=String)
    phone = Column(type_=String)
    created_at = Column(type_=DateTime, server_default=func.now())

    rooms = relationship(argument=HotelHasRoom)
    extras = relationship(argument=HotelHasExtra)

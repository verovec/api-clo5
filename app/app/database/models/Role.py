from sqlalchemy import Column, Integer, String

from database.database import Base


class Role(Base):
    __tablename__ = "Role"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)
    label = Column(String, nullable=False)

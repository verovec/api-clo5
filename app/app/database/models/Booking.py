from sqlalchemy import Column, DateTime, ForeignKey, Integer, func
from sqlalchemy.orm import relationship

from database.database import Base
from database.models.Room import Room
from database.models.Extra import Extra
from database.models.User import User
from database.models.Hotel import Hotel


class BookingHasExtra(Base):
    __tablename__ = "BookingHasExtra"

    id = Column(type_=Integer, primary_key=True, index=True)
    booking_id = Column(Integer, ForeignKey(column="Booking.id"))
    extra_id = Column(Integer, ForeignKey(column=Extra.id))
    number = Column(type_=Integer)

    booking = relationship(argument="Booking", back_populates="extras")
    extra = relationship(argument=Extra)


class BookingHasRoom(Base):
    __tablename__ = "BookingHasRoom"

    id = Column(type_=Integer, primary_key=True, index=True)
    booking_id = Column(Integer, ForeignKey(column="Booking.id"))
    room_id = Column(Integer, ForeignKey(column=Room.id))
    number = Column(type_=Integer)

    booking = relationship(argument="Booking", back_populates="rooms")
    room = relationship(argument=Room)


class Booking(Base):
    __tablename__ = "Booking"

    id = Column(type_=Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey(column=User.id))
    hotel_id = Column(Integer, ForeignKey(column=Hotel.id))
    begin_date = Column(type_=DateTime)
    end_date = Column(type_=DateTime)
    created_at = Column(type_=DateTime, server_default=func.now())

    user = relationship(argument=User)
    rooms = relationship(argument=BookingHasRoom)
    extras = relationship(argument=BookingHasExtra)

from sqlalchemy import Column, DateTime, Float, Integer, String, func

from database.database import Base


class Extra(Base):
    __tablename__ = "Extra"

    id = Column(type_=Integer, primary_key=True, index=True)
    name = Column(type_=String)
    price = Column(type_=Float)
    advance = Column(type_=Integer)
    created_at = Column(type_=DateTime, server_default=func.now())

from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship

from database.database import Base
from database.models.User import User


class Token(Base):
    __tablename__ = 'Token'

    id = Column(Integer, primary_key=True, autoincrement=True)
    ip = Column(String(64), nullable=True)
    token = Column(Text, nullable=False)
    user_id = Column(Integer, ForeignKey(column=User.id), nullable=False)
    ut_created_at = Column(Integer, nullable=False)
    ut_expires_at = Column(Integer, nullable=False)

    user = relationship(User)

from sqlalchemy import Column, DateTime, Float, Integer, String, func

from database.database import Base


class Room(Base):
    __tablename__ = "Room"

    id = Column(type_=Integer, primary_key=True, index=True)
    small_name = Column(type_=String)
    name = Column(type_=String)
    size = Column(type_=Integer)
    price = Column(type_=Float)
    created_at = Column(type_=DateTime, server_default=func.now())

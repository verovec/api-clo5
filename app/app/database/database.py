from os import getenv
from typing import Any

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL: str = "postgresql://{}:{}@{}/{}".format(
    getenv("POSTGRES_USER"),
    getenv("POSTGRES_PASSWORD"),
    getenv("POSTGRES_HOST"),
    getenv("POSTGRES_DB"),
)

engine: Any = create_engine(
    SQLALCHEMY_DATABASE_URL
)
SessionLocal: sessionmaker = sessionmaker(
    autocommit=False, autoflush=False, bind=engine)

Base: Any = declarative_base()

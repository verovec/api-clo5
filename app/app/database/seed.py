from collections import Counter
from typing import List, Optional, TypedDict
from sqlalchemy.orm import Session

from database.dependencies import get_db
from database.models.User import User
from database.models.Room import Room
from database.models.Extra import Extra
from database.models.Role import Role
from database.models.Hotel import Hotel, HotelHasExtra, HotelHasRoom


class RoleSchema(TypedDict):
    id: int
    name: str
    label: str


def seedRoles(db: Session) -> None:
    roles: List[RoleSchema] = [
        {"id": 1, "name": "admin", "label": "Administrateur"},
        {"id": 2, "name": "manager", "label": "Manager"},
        {"id": 3, "name": "user", "label": "User"},
    ]

    for role in roles:
        role_db: Optional[Role] = db.query(Role).get(ident=role["id"])
        if role_db is None:
            role_db = Role(**role)

            db.add(instance=role_db)
            db.commit()


def seedAdminUser(db: Session) -> None:
    user: Optional[User] = db.query(User).get(ident=1)
    if user is None:
        user = User(
            id=1,
            role_id=1,
            email="admin@booking.com",
            phone="+33689584587",
            firstname="Admin",
            lastname="Admin"
        )

        user.set_password(password="test")

        db.add(instance=user)
        db.commit()


class RoomSchema(TypedDict):
    id: int
    small_name: str
    name: str
    size: int
    price: float


def seedRooms(db: Session) -> None:
    rooms: List[RoomSchema] = [
        {"id": 1, "small_name": "SR", "name": "Suite présidentielle",
            "size": 5, "price": 1000},
        {"id": 2, "small_name": "S", "name": "Suite",
            "size": 3, "price": 720},
        {"id": 3, "small_name": "JS", "name": "Junior Suite",
            "size": 2, "price": 500},
        {"id": 4, "small_name": "CD", "name": "Chambre de luxe",
            "size": 2, "price": 300},
        {"id": 5, "small_name": "CS", "name": "Chambre standard",
            "size": 2, "price": 150},
    ]
    for room in rooms:
        room_db: Optional[Room] = db.query(Room).get(ident=room["id"])
        if room_db is None:
            room_db = Room(**room)

            db.add(instance=room_db)
            db.commit()


class ExtraSchema(TypedDict):
    id: int
    name: str
    price: float
    advance: int


def seedExtras(db: Session) -> None:
    extras: List[ExtraSchema] = [
        {"id": 1, "name": "Place de garage",
            "price": 25, "advance": 0},
        {"id": 2, "name": "Ajout d'un lit bébé",
            "price": 0, "advance": 0},
        {"id": 3, "name": "Pack romance",
            "price": 50, "advance": 2},
        {"id": 4, "name": "Petit déjeuner",
            "price": 30, "advance": 0}
    ]
    for extra in extras:
        extra_db: Optional[Extra] = db.query(Extra).get(ident=extra["id"])
        if extra_db is None:
            extra_db = Extra(**extra)

            db.add(instance=extra_db)
            db.commit()


class HotelSchema(TypedDict):
    id: int
    address: str
    phone: str
    rooms_ids: List[int]
    extras_ids: List[int]


def seedHotels(db: Session) -> None:
    hotels: List[HotelSchema] = [
        {"id": 1, "address": "Paris",
            "phone": "0175847361", "rooms_ids": [2, 3, 4, 5, 5], "extras_ids": [1, 1, 1, 2, 2]},
        {"id": 2, "address": "Toulouse",
            "phone": "0190353783", "rooms_ids": [1, 1], "extras_ids": [1, 1, 2, 2]}
    ]
    for hotel in hotels:
        hotel_db: Optional[Hotel] = db.query(Hotel).get(ident=hotel["id"])
        if hotel_db is None:
            hotel_db = Hotel(address=hotel["address"], phone=hotel["phone"])

            rooms_ids: Counter = Counter(hotel["rooms_ids"])
            for room_id in rooms_ids.most_common():
                hotel_db.rooms.append(HotelHasRoom(
                    room_id=room_id[0], number=room_id[1]))
            extras_ids: Counter = Counter(hotel["extras_ids"])
            for extra_id in extras_ids.most_common():
                hotel_db.extras.append(HotelHasExtra(
                    extra_id=extra_id[0], number=extra_id[1]))

            db.add(instance=hotel_db)
            db.commit()


def seed() -> None:
    db: Session = list(get_db())[0]

    seedRoles(db=db)
    seedAdminUser(db=db)
    seedRooms(db=db)
    seedExtras(db=db)
    seedHotels(db=db)

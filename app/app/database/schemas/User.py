from datetime import datetime
from pydantic import BaseModel


class UserBase(BaseModel):
    email: str
    phone: str
    firstname: str
    lastname: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    created_at: datetime

    class Config:
        orm_mode = True

from typing import List
from pydantic import BaseModel


class HotelBase(BaseModel):
    address: str
    phone: str


class HotelCreate(HotelBase):
    rooms_ids: List[int]
    extras_ids: List[int]


class Hotel(HotelBase):
    id: int

    class Config:
        orm_mode = True

from pydantic import BaseModel


class RoomBase(BaseModel):
    small_name: str
    name: str
    size: int
    price: float


class RoomCreate(RoomBase):
    pass


class Room(RoomBase):
    id: int

    class Config:
        orm_mode = True

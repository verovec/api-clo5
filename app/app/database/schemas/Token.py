from pydantic import BaseModel

from database.schemas.User import User


class TokenBase(BaseModel):
    token: str


class Token(TokenBase):
    id: int
    user: User

    class Config:
        orm_mode = True

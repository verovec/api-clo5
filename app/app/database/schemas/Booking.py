from datetime import datetime
from typing import List
from pydantic import BaseModel


class BookingBase(BaseModel):
    hotel_id: int


class BookingCreate(BookingBase):
    rooms_ids: List[int]
    extras_ids: List[int]
    begin_date: datetime
    end_date: datetime


class Booking(BookingBase):
    id: int

    class Config:
        orm_mode = True

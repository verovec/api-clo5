from fastapi import FastAPI, Depends

from database.dependencies import need_authentication
from database.seed import seed
from database.models import *
from database.database import engine, Base
from routers import auth, hotel, booking, room

Base.metadata.create_all(bind=engine)

app: FastAPI = FastAPI()

seed()

app.include_router(router=auth.router, prefix="/auth", tags=["auth"])
app.include_router(router=hotel.router, prefix="/hotel",
                   tags=["hotel"], dependencies=[Depends(need_authentication)])
app.include_router(router=room.router, prefix="/room",
                   tags=["room"], dependencies=[Depends(need_authentication)])
app.include_router(router=booking.router, prefix="/booking",
                   tags=["booking"], dependencies=[Depends(need_authentication)])


@app.get(path="/", response_model=dict[str, str])
def read_root() -> dict[str, str]:
    return {"status": "running"}

FROM python:3.9

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt
COPY ./app /app

RUN pip install --upgrade -r /app/requirements.txt

RUN pip install --upgrade pytest==7.1.3 requests==2.28.1

CMD ["pytest"]
